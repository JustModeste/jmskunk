<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>HeaderForm.ui</name>
    <message>
        <location filename="../qml/HeaderForm.ui.qml" line="19"/>
        <source>back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../qml/HeaderForm.ui.qml" line="30"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>JMItemForm.ui</name>
    <message>
        <location filename="../qml/JMItemForm.ui.qml" line="33"/>
        <source>Label</source>
        <translation>Label</translation>
    </message>
</context>
<context>
    <name>PlayerTableForm.ui</name>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="36"/>
        <source>Roll</source>
        <translation>Lancer</translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="43"/>
        <source>Stop</source>
        <translation>Stoper</translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="52"/>
        <source>Win</source>
        <translation>Gagné</translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="58"/>
        <source>Lose</source>
        <translation>Perdu</translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="98"/>
        <source>You WIN</source>
        <translation>Vous avez GAGNÉ</translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="108"/>
        <source>You LOSE</source>
        <translation>You avez PERDU</translation>
    </message>
</context>
<context>
    <name>RulesForm.ui</name>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="17"/>
        <source>Rules</source>
        <translation>Règles</translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="24"/>
        <source>The object of the game is to be the first player to score 100 points (or more) and not have their total score beaten.</source>
        <translation>Le but du jeu est d&apos;être le premier jouer a marqué 100 points (ou plus) et de ne pas se faire battre.</translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="32"/>
        <source>A player scores points by rolling the dice and adding together their total. For example if a player rolls a 6 and 5, their score would be 11.  They can choose to roll again and continue to add to this score, taking the risk of rolling a bear, or they can stop and keep the score of 11 for their first round.</source>
        <translation>Un joueur marque des points en lançant les dés et en additionnant leur total. Par exemple, si un joueur lance un 6 et un 5, son score serait de 11. Ils peuvent choisir de lancer à nouveau et de continuer à ajouter à ce score, en prenant le risque de lancer un ours, ou ils peuvent s&apos;arrêter et garder le score de 11 pour leur premier tour.</translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="40"/>
        <source>Rolling a bear or double bears during a players turn has consequences.</source>
        <translation>Lancer un ours ou un double ours pendant un tour de joueur a des conséquences.</translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="48"/>
        <source>If the player rolls one bear at anytime during their turn, they receive 0 points for the round.</source>
        <translation>Si un joueur tire un ours à n&apos;importe quel instant de son tour, il reçoit 0 point pour le tour.</translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="56"/>
        <source>If a player rolls double bears at anytime during their turn, they receive a 0 for the round and all the previous rounds.  They need to start all over again in accumulating points.</source>
        <translation>Si un joueur tire un double ours durant son tour, il reçoit 0 pour ce tour et son total est remis à 0. Il doit recommencer depuis le début pour accumuler points.</translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="62"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>SelectUsersForm.ui</name>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="39"/>
        <location filename="../qml/SelectUsersForm.ui.qml" line="40"/>
        <source>Player 1</source>
        <translation>Joueur 1</translation>
    </message>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="48"/>
        <location filename="../qml/SelectUsersForm.ui.qml" line="49"/>
        <source>Player 2</source>
        <translation>Joueur 2</translation>
    </message>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="58"/>
        <source>Rules</source>
        <translation>Règles</translation>
    </message>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="66"/>
        <source>Play</source>
        <translation>Jouer</translation>
    </message>
</context>
<context>
    <name>TableForm.ui</name>
    <message>
        <location filename="../qml/TableForm.ui.qml" line="45"/>
        <location filename="../qml/TableForm.ui.qml" line="51"/>
        <source>Pot</source>
        <translation>Pot</translation>
    </message>
</context>
<context>
    <name>UserNameForm.ui</name>
    <message>
        <location filename="../qml/UserNameForm.ui.qml" line="28"/>
        <source>Player name</source>
        <translation>Nom du joueur</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="10"/>
        <source>jmSkunK</source>
        <translation>jmSkunk</translation>
    </message>
</context>
</TS>
