<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>HeaderForm.ui</name>
    <message>
        <location filename="../qml/HeaderForm.ui.qml" line="19"/>
        <source>back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/HeaderForm.ui.qml" line="30"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JMItemForm.ui</name>
    <message>
        <location filename="../qml/JMItemForm.ui.qml" line="33"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerTableForm.ui</name>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="36"/>
        <source>Roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="43"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="52"/>
        <source>Win</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="58"/>
        <source>Lose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="98"/>
        <source>You WIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PlayerTableForm.ui.qml" line="108"/>
        <source>You LOSE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RulesForm.ui</name>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="17"/>
        <source>Rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="24"/>
        <source>The object of the game is to be the first player to score 100 points (or more) and not have their total score beaten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="32"/>
        <source>A player scores points by rolling the dice and adding together their total. For example if a player rolls a 6 and 5, their score would be 11.  They can choose to roll again and continue to add to this score, taking the risk of rolling a bear, or they can stop and keep the score of 11 for their first round.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="40"/>
        <source>Rolling a bear or double bears during a players turn has consequences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="48"/>
        <source>If the player rolls one bear at anytime during their turn, they receive 0 points for the round.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="56"/>
        <source>If a player rolls double bears at anytime during their turn, they receive a 0 for the round and all the previous rounds.  They need to start all over again in accumulating points.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RulesForm.ui.qml" line="62"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectUsersForm.ui</name>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="39"/>
        <location filename="../qml/SelectUsersForm.ui.qml" line="40"/>
        <source>Player 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="48"/>
        <location filename="../qml/SelectUsersForm.ui.qml" line="49"/>
        <source>Player 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="58"/>
        <source>Rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/SelectUsersForm.ui.qml" line="66"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableForm.ui</name>
    <message>
        <location filename="../qml/TableForm.ui.qml" line="45"/>
        <location filename="../qml/TableForm.ui.qml" line="51"/>
        <source>Pot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserNameForm.ui</name>
    <message>
        <location filename="../qml/UserNameForm.ui.qml" line="28"/>
        <source>Player name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="10"/>
        <source>jmSkunK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
