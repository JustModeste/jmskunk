#include "game.h"

Game::Game(Player *player1, Player *player2, int maxScore, QObject *parent)
    : QObject{parent}
{
    this->_player1 = player1;
    this->_player2 = player2;
    this->_maxScore = maxScore;
    this->_pot = 0;
    this->_currentPlayer = 1;
    this->_finished = false;
    this->_timer = new QTimer(this);
    this->_timer->setSingleShot(true);
    connect(this->_timer, &QTimer::timeout, this, QOverload<>::of(&Game::updateTimer));
}


Player *Game::player1() const
{
    return this->_player1;
}

void Game::setPlayer1(Player *newPlayer1)
{
    if (this->_player1 == newPlayer1)
        return;
    this->_player1 = newPlayer1;
    emit player1Changed();
}

Player *Game::player2() const
{
    return this->_player2;
}

void Game::setPlayer2(Player *newPlayer2)
{
    if (this->_player2 == newPlayer2)
        return;
    this->_player2 = newPlayer2;
    emit player1Changed();
}

int Game::maxScore() const
{
    return this->_maxScore;
}

void Game::setMaxScore(int newMaxScore)
{
    if (this->_maxScore == newMaxScore)
        return;
    this->_maxScore = newMaxScore;
    emit maxScoreChanged();
}

int Game::pot() const
{
    return this->_pot;
}

void Game::setPot(int newPot)
{
    if (this->_pot == newPot)
        return;
    this->_pot = newPot;
    emit potChanged();
}

int Game::currentPlayer() const
{
    return this->_currentPlayer;
}

void Game::resetPlayer(Player *player) {
    player->setScore(0);
}

void Game::addPotPlayer(Player *player)
{
    player->setScore(player->score() + this->_pot);
    this->setPot(0);
    if (player == this->_player1) {
        this->_finished = this->_player2->score() >= this->maxScore();
    } else {
        this->_finished = this->_player1->score() >= this->maxScore();
    }
}

Player *Game::getPlayer() const
{
    if (this->_currentPlayer == 1) {
        return this->_player1;
    }
    return this->_player2;
}

void Game::updateTimer()
{
    qDebug() << "Timer...";
    Player *player = this->getPlayer();
    player->play(this->_pot);
}

void Game::changePlayer(bool reset)
{
    Player *player = this->getPlayer();

    addPotPlayer(player);
    if (reset) {
        resetPlayer(player);
    }
    if (this->_currentPlayer == 1) {
        this->_currentPlayer = 2;
        emit currentPlayerChanged();
    } else {
        this->_currentPlayer = 1;
        emit currentPlayerChanged();
    }
}

void Game::playerPlay()
{
    Player *player = this->getPlayer();
    if (player->isIA()) {
        qDebug() << "IA joue...";
        this->_timer->start(500);
    } else {
        this->_timer->stop();
    }
}
