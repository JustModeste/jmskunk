#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QTimer>
#include <QDebug>

#include "src/player.h"

class Game : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Player *player1 READ player1 WRITE setPlayer1 NOTIFY player1Changed)
    Q_PROPERTY(Player *player2 READ player2 WRITE setPlayer2 NOTIFY player2Changed)
    Q_PROPERTY(int maxScore READ maxScore WRITE setMaxScore NOTIFY maxScoreChanged)
    Q_PROPERTY(int pot READ pot WRITE setPot NOTIFY potChanged)
    Q_PROPERTY(int currentPlayer READ currentPlayer NOTIFY currentPlayerChanged)

private:
    Player  *_player1;
    Player  *_player2;
    int     _maxScore;
    int     _pot;
    int     _currentPlayer;
    bool    _finished;
    QTimer  *_timer;

    void resetPlayer(Player *player);
    void addPotPlayer(Player *player);
    Player *getPlayer() const;

    void updateTimer(void);
public:

    explicit Game(Player *player1, Player *player2, int maxScore, QObject *parent = nullptr);

    Player *player1() const;
    void setPlayer1(Player *newPlayer1);

    Player *player2() const;
    void setPlayer2(Player *newPlayer2);

    int maxScore() const;
    void setMaxScore(int newMaxScore);

    int pot() const;
    void setPot(int newPot);

    int currentPlayer() const;
    Q_INVOKABLE void changePlayer(bool reset = false);
    Q_INVOKABLE void playerPlay();
signals:
    void player1Changed();
    void player2Changed();
    void maxScoreChanged();
    void potChanged();
    void currentPlayerChanged();
};

#endif // GAME_H
