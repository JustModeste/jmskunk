#include "ia.h"

IA::IA(QObject *parent)
    : QObject{parent}
{

}

IA::Action IA::play(int currentPot)
{
    this->m_nbStep++;
qDebug() << "Step: " << this->m_nbStep << " - Pot: " << currentPot << "Moy:" << (currentPot / this->m_nbStep);
    if (currentPot == 0) {
        qDebug() << "Premier lancer";
        return IA::ROLL;
    } else if (this->m_nbStep > 2 && (currentPot / (this->m_nbStep - 1) <= 7) && (this->m_nbStep < 3)) {
        qDebug() << "Petit Score";
        return IA::ROLL;
    }
    return IA::PASS;

}

void IA::setNbStep(int newNbStep)
{
    if (m_nbStep == newNbStep)
        return;
    m_nbStep = newNbStep;
    emit nbStepChanged();
}
