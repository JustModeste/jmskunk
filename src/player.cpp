#include "player.h"

Player::Player(QString name, bool isIA, QObject *parent)
    : QObject{parent}
{
    this->_name = name;
    this->_score = 0;
    this->_nbWin = 0;
    this->_nbLose = 0;
    this->_isIA = isIA;
    if (isIA) {
        this->_ia = new IA(parent);
    } else {
        this->_ia = NULL;
    }
}

QString Player::name() const
{
    return this->_name;
}

void Player::setName(QString newName)
{
    if (this->_name == newName) {
        return;
    }
    this->_name = newName;
    emit nameChanged();
}

int Player::score() const
{
    return this->_score;
}

void Player::setScore(int newScore)
{
    if (this->_score == newScore)
        return;
    _score = newScore;
    if (this->_isIA) {
        this->_ia->setNbStep(0);
    }
    emit scoreChanged(newScore);
}

int Player::nbWin() const
{
    return this->_nbWin;
}

void Player::setNbWin(int newNbWin)
{
    if (this->_nbWin == newNbWin)
        return;
    this->_nbWin = newNbWin;
    emit nbWinChanged();
}

int Player::nbLose() const
{
    return this->_nbLose;
}

void Player::setNbLose(int newNbLose)
{
    if (this->_nbLose == newNbLose)
        return;
    this->_nbLose = newNbLose;
    emit nbLoseChanged();
}

bool Player::isIA() const
{
    return this->_isIA;
}

void Player::setIsIA(bool newIsIa)
{
    if (this->_isIA == newIsIa)
        return;
    this->_isIA = newIsIa;
    delete this->_ia;
    if (newIsIa) {
        this->_ia = new IA(this);
    }
    emit isIAChanged();
}

void Player::play(int currentPot)
{
    IA::Action action = IA::END;

    if (this->_isIA && this->_ia != NULL) {
         action = this->_ia->play(currentPot);
    }
    qDebug() << "Action:" << action;
    emit actionChanged(action);

}
