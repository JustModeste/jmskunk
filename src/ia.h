#ifndef IA_H
#define IA_H

#include <QObject>

#include <QDebug>

class IA : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int nbStep WRITE setNbStep NOTIFY nbStepChanged)
    int m_nbStep;

public:
    enum Action {
        ROLL = 0,
        PASS,
        END
    };

    explicit IA(QObject *parent = nullptr);

    Q_INVOKABLE IA::Action  play(int currentPot);

    void setNbStep(int newNbStep);

signals:

    void nbStepChanged();
};

#endif // IA_H
