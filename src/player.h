#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QString>
#include <QDebug>

#include "src/ia.h"

class Player : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int score READ score WRITE setScore NOTIFY scoreChanged)
    Q_PROPERTY(int nbWin READ nbWin WRITE setNbWin NOTIFY nbWinChanged)
    Q_PROPERTY(int nbLose READ nbLose WRITE setNbLose NOTIFY nbLoseChanged)
    Q_PROPERTY(bool isIA READ isIA WRITE setIsIA NOTIFY isIAChanged)

private:
    QString _name;
    int     _score;
    int     _nbWin;
    int     _nbLose;
    bool    _isIA;
    IA      *_ia;

public:

    explicit Player(QString name, bool isIA = false, QObject *parent = nullptr);

    QString name(void) const;
    void setName(QString newName);

    int score(void) const;
    void setScore(int newScore);

    int nbWin() const;
    void setNbWin(int newNbWin);

    int nbLose() const;
    void setNbLose(int newNbLose);

    bool isIA() const;
    void setIsIA(bool newIsIa);

    Q_INVOKABLE void play(int currentPot);
signals:
    void nameChanged();
    void scoreChanged(int value);
    void nbWinChanged();
    void nbLoseChanged();
    void isIAChanged();
    void actionChanged(IA::Action action);
};

#endif // PLAYER_H
