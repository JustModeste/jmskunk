import QtQuick 2.15
import QtQuick.Layouts 1.15

Item {
    width: 400
    height: 60

    property alias bBack: bBack
    property alias bQuit: bQuit

    RowLayout {
        id: row
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        JMButton {
            id: bBack
            text: qsTr("back")
        }

        Text {
            id: title
            text: "jmSkunK"
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }
        JMButton {
            id: bQuit
            text: qsTr("Quit")
        }
    }
}
