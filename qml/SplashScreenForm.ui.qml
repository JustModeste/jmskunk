import QtQuick 2.0
import QtQuick.Layouts 1.15

Rectangle {
    id: rectangle
    width: 454
    height: 800
    border.color: "#ffffff"

    ColumnLayout {
        id: row
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        spacing: 10


        Image {
            id: title
            width: 285
            height: 68
            source: "qrc:/images/jmSkunK.png"
            Layout.bottomMargin: 100
        }

        Rectangle {
            width: 282
            height: 240
            color: "#ff00ff"

            Image {
                anchors.fill: parent
                source: "qrc:/images/logo.png"
            }
        }

        Text {
            id: legend
            text: "logo by Perrine Adam"
            horizontalAlignment: Text.AlignRight
            Layout.fillWidth: true
        }
    }
}


