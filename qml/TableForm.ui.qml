import QtQuick 2.15
import QtQuick.Layouts 1.15

Rectangle {
    width: 460
    height: 844
    color: "#ededed"
    border.color: "#ffffff"

    property alias player1: player1
    property alias player2: player2
    property alias dice1: dice1
    property alias dice2: dice2

    ColumnLayout {
        anchors.fill: parent
        spacing: 5

        PlayerTable {
            id: player2
            Layout.fillWidth: true
            Layout.rightMargin: 10
            Layout.leftMargin: 10
            rotation: 180
            color: "#f3574e"

            playerName: (game !== null) ? game.player2.name : ""
            playerScore: (game !== null) ? game.player2.score : ""
            nbWin: (game !== null) ? game.player2.nbWin : ""
            nbLose: (game !== null) ? game.player2.nbLose : ""
        }

        Row {
            id: row
            Layout.alignment: Qt.AlignHCenter
            spacing: 20
            Dice {
                id: dice1
                anchors.verticalCenter: parent.verticalCenter
                dice.source: "qrc:/images/01.png"
            }

            ColumnLayout {
                ScorePlayer {
                    label: qsTr("Pot")
                    width: 100
                    score: (game !== null) ? game.pot : ""
                    rectScore.bottomColor: "#4f71f6"
                }
                ScorePlayer {
                    label: qsTr("Pot")
                    width: 100
                    score: (game !== null) ? game.pot : ""
                    rotation: 180
                    rectScore.bottomColor: "#f3574e"
                }
            }

            Dice {
                id: dice2
                anchors.verticalCenter: parent.verticalCenter
                dice.source: "qrc:/images/01.png"
                rotation: 180
            }
        }

        PlayerTable {
            id: player1
            color: "#4f71f6"
            Layout.fillWidth: true
            Layout.rightMargin: 10
            Layout.leftMargin: 10

            playerName: (game !== null) ? game.player1.name : ""
            playerScore: (game !== null) ? game.player1.score : ""
            nbWin: (game !== null) ? game.player1.nbWin : ""
            nbLose: (game !== null) ? game.player1.nbLose : ""
        }
    }
}
