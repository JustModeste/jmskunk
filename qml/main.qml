import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Window {
    id: window
    width: 454
    height: 800
    visible: true
    title: qsTr("jmSkunK")

    property int pSplashScreen: 0
    property int pSelectUsers: 1
    property int pTable: 2
    property int pRules: 3

    StackLayout {
        id: stack
        anchors.fill: parent
        currentIndex: pSplashScreen

        SplashScreen {
            Timer {
                interval: 2000
                running: true
                onTriggered: {
                    running: false
                    stack.currentIndex = pSelectUsers
                }
            }
        }

        SelectUsers {
            id: selectUsers
            bPlay.onClicked: {
                if (player1Name !== "") {
                    game.player1.name = player1Name
                    game.player1.isIA = player1isIA
                }
                if (player2Name !== "") {
                    game.player2.name = player2Name
                    game.player2.isIA = player2isIA
                }
                stack.currentIndex = pTable
            }
            bRules.onClicked: {
                stack.currentIndex = pRules
            }
        }

        Table {
            id: table
            onVisibleChanged: {
                if (visible) {
                    table.startNewGame()
                }
            }
        }

        Rules {
            id: rules

            bOk.onClicked: {
                stack.currentIndex = pSelectUsers
            }
        }
    }
}
