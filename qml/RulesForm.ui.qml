import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Item {
    id: iRoot

    property alias bOk: bOk

    Column {
        id: column
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        JMItem {
            text: qsTr("Rules")
            width: iRoot.width - 20
        }

        TextArea {
            width: iRoot.width - 20
            color: "#000000"
            text: qsTr("The object of the game is to be the first player to score 100 points (or more) and not have their total score beaten.")
            wrapMode: Text.WordWrap
            enabled: false
        }

        TextArea {
            width: iRoot.width - 20
            color: "#000000"
            text: qsTr("A player scores points by rolling the dice and adding together their total. For example if a player rolls a 6 and 5, their score would be 11.  They can choose to roll again and continue to add to this score, taking the risk of rolling a bear, or they can stop and keep the score of 11 for their first round.")
            wrapMode: Text.WordWrap
            enabled: false
        }

        TextArea {
            width: iRoot.width - 20
            color: "#000000"
            text: qsTr("Rolling a bear or double bears during a players turn has consequences.")
            wrapMode: Text.WordWrap
            enabled: false
        }

        TextArea {
            width: iRoot.width - 20
            color: "#000000"
            text: qsTr("If the player rolls one bear at anytime during their turn, they receive 0 points for the round.")
            wrapMode: Text.WordWrap
            enabled: false
        }

        TextArea {
            width: iRoot.width - 20
            color: "#000000"
            text: qsTr("If a player rolls double bears at anytime during their turn, they receive a 0 for the round and all the previous rounds.  They need to start all over again in accumulating points.")
            wrapMode: Text.WordWrap
            enabled: false
        }
        JMButton {
            id: bOk
            text: qsTr("OK")
            width: iRoot.width - 20
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

