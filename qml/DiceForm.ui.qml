import QtQuick 2.4

Item {
    width: 100
    height: 100

    property alias dice: dice


    JMImgButton {
        id: dice
        anchors.fill: parent
        source: "qrc:/images/01.png"
        color: colorClick
    }
}
