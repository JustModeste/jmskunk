import QtQuick 2.4

DiceForm {
    property int numberDraw: 0
    property int value: 1

    signal finished


    Timer {
        id: timer
        interval: 100
        repeat: true
        running: false
        onTriggered: {
            if (numberDraw > 0) {
                dice.color = dice.colorUnclick;
                var newValue = value;
                do {
                    newValue = Math.floor(Math.random() * 6) + 1;
                } while (newValue == value);
                value = newValue;

                let image = "qrc:/images/0" + value + ".png";

                dice.source = image;
            } else {
                timer.stop();
                dice.color = dice.colorClick ;
                finished();
            }

            numberDraw--;


        }
    }

    function draw() {
        numberDraw = Math.floor(Math.random() * 20) + 1;

        timer.start();
    }

}
