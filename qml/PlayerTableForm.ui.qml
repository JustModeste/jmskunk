import QtQuick 2.4
import QtQuick.Layouts 1.15

Item {
    id: iRoot
    width: 480
    height: 250

    property alias playerName: player.label
    property alias playerScore: player.score
    property alias bRoll: bRoll
    property alias bPass: bPass
    property alias bEnd: bEnd
    property alias nbWin: win.score
    property alias nbLose: lose.score
    property string color: "#ffffff"

    Column {
        id: column
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10

        ScorePlayer {
            id: player
            width: (iRoot.width - 40)
            rectScore.bottomColor: color
        }

        Row {
            spacing: 10
            JMButton {
                id: bRoll
                enabled: false
                width: (iRoot.width - 50) / 2
                text: qsTr("Roll")
            }

            JMButton {
                id: bPass
                enabled: false
                width: (iRoot.width - 50) / 2
                text: qsTr("Stop")
            }
        }
        Row {
            height: 50
            spacing: 10
            ScorePlayer {
                id: win
                width: (iRoot.width - 50) / 2
                label: qsTr("Win")
                rectScore.bottomColor: color
            }
            ScorePlayer {
                id: lose
                width: (iRoot.width - 50) / 2
                label: qsTr("Lose")
                rectScore.bottomColor: color
            }
        }
    }
    JMButton {
        id: bEnd
        visible: false
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        width: iRoot.width - 80
        height: iRoot.height - 80
    }

    states: [
        State {
            name: "Selected"

            PropertyChanges {
                target: bPass
                enabled: true
            }

            PropertyChanges {
                target: bRoll
                enabled: true
            }

            PropertyChanges {
                target: player
                state: "Selected"
            }
        },
        State {
            name: "Win"

            PropertyChanges {
                target: bEnd
                visible: true
                bottomColor: "#1dfb32"
                text: qsTr("You WIN")
            }
        },
        State {
            name: "Lose"

            PropertyChanges {
                target: bEnd
                visible: true
                bottomColor: "#fb1d1d"
                text: qsTr("You LOSE")
            }
        }
    ]
}
