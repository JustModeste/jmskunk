import QtQuick 2.4

TableForm {
    property int nbDiceFinished: 0
    property int currentPlayer: (game != null) ? game.currentPlayer : 0


    Connections {
        target: game
        function onPotChanged() {
            console.log("Pot Changed");
        }
    }

    Connections {
        target: (game != null) ? game.player1 : null
        function onActionChanged(action) {
            if (!(action > 0)) {
                action = 0
            }

            console.log("action :");

            console.log(action);
            console.log("ici");
            console.log(action > 0)
            if (action === 0 || action === undefined) {
                rollDices();
            } else if (action === 1) {
                game.changePlayer();
            }
        }
    }

    Connections {
        target: (game != null) ? game.player2 : null
        function onActionChanged(action) {
            if (!(action > 0)) {
                action = 0
            }
            console.log("action :");
            console.log(action || action === undefined);
            if (action === 0) {
                rollDices();
            } else if (action === 1) {
                game.changePlayer();
            }
        }
    }

    function rollDices() {
        if (currentPlayer === 1) {
            player1.bRoll.enabled = false
            player1.bPass.enabled = false
        } else {
            player2.bRoll.enabled = false
            player2.bPass.enabled = false
        }
        nbDiceFinished = 0
        dice1.draw()
        dice2.draw()
    }

    function calculatePot() {
        if (nbDiceFinished == 2) {

            if (dice1.value !== 1 && dice2.value !== 1) {
                game.pot += dice1.value + dice2.value
                game.playerPlay();
            } else {
                game.pot = 0;
                var resetPlayer = false;
                if (dice1.value === 1 && dice2.value === 1) {
                    resetPlayer = true;
                }
                game.changePlayer(resetPlayer);
            }
            if (currentPlayer === 1) {
                player1.bRoll.enabled = true
                player1.bPass.enabled = true
            } else {
                player2.bRoll.enabled = true
                player2.bPass.enabled = true
            }
        }
    }

    function startNewGame() {
        console.log("NewGame");
        if (game.player1.isIA && game.player2.isIA) {
            nbDiceFinished = 2;
        }

        if (nbDiceFinished == 2) {
            game.player1.score = 0;
            game.player2.score = 0;
            if (currentPlayer === 1) {
                player1.state = "Selected";
                player2.state = "";
            } else {
                player1.state = "";
                player2.state = "Selected";
            }
            game.playerPlay();
        }
    }

    onCurrentPlayerChanged: {
        let finished = (game != null) ? Math.max(game.player1.score, game.player2.score) >= game.maxScore : false;

        if (finished) {
            if (game.player1.isIA && game.player2.isIA) {
                if (game.player1.score >= game.maxScore) {
                    game.player1.nbWin++;
                    game.player2.nbLose++;
                } else {
                    game.player1.nbLose++;
                    game.player2.nbWin++;
                }
                startNewGame();
            } else {

                nbDiceFinished = 0;
                if (game.player1.score >= game.maxScore) {
                    game.player1.nbWin++;
                    game.player2.nbLose++;
                    player1.state = "Win";
                    player2.state = "Lose";
                } else {
                    game.player1.nbLose++;
                    game.player2.nbWin++;
                    player1.state = "Lose";
                    player2.state = "Win";
                }
            }
        } else {
            if (currentPlayer == 1) {
                player1.state = "Selected";
                player2.state = "";
            } else {
                player1.state = "";
                player2.state = "Selected";
            }
            if (game != null) {
                game.playerPlay();
            }
        }
    }

    player1.bPass.onClicked: {
        game.changePlayer();
    }

    player1.bRoll.onClicked: {
        rollDices()
    }

    player2.bPass.onClicked:  {
        game.changePlayer();
    }

    player2.bRoll.onClicked: {
        rollDices()
    }

    dice1.onFinished: {
        nbDiceFinished++;
        calculatePot();
    }

    dice2.onFinished: {
        nbDiceFinished++;
        calculatePot();
    }

    player1.bEnd.onClicked: {
        nbDiceFinished++;
        if (game.player2.isIA && nbDiceFinished == 1) {
            player2.bEnd.clicked()
        }

        player1.state = "";
        startNewGame();
    }

    player2.bEnd.onClicked: {
        nbDiceFinished++;
        player2.state = "";
        startNewGame();
    }
}
