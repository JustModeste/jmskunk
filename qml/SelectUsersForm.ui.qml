import QtQuick 2.15
import QtQuick.Layouts 1.15

Item {
    id: item1
    width: 400
    height: 400

    property alias bPlay: bPlay
    property alias bRules: bRules
    property alias player1Name: player1.name
    property alias player1isIA: player1.isIA
    property alias player2Name: player2.name
    property alias player2isIA: player2.isIA

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            spacing: 10
            Rectangle {
                id: logo
                width: 56
                height: 48

                Image {
                    anchors.fill: parent
                    source: "qrc:/images/logo.png"
                }
            }
            Image {
                source: "qrc:/images/jmSkunK.png"
            }
        }

        UserName {
            id: player1
            label: qsTr("Player 1")
            name: qsTr("Player 1")
            Layout.rightMargin: 10
            Layout.leftMargin: 10
            Layout.fillWidth: true
        }

        UserName {
            id: player2
            label: qsTr("Player 2")
            name: qsTr("Player 2")
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.fillWidth: true
            isIA: true
        }

        JMButton {
            id: bRules
            text: qsTr("Rules")
            Layout.fillWidth: true
            Layout.rightMargin: 10
            Layout.leftMargin: 10
        }

        JMButton {
            id: bPlay
            text: qsTr("Play")
            Layout.fillWidth: true
            Layout.rightMargin: 10
            Layout.leftMargin: 10
        }
    }
}
