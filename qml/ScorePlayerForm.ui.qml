import QtQuick 2.4

Item {
    id: iRoot
    width: 460
    height: 60

    property alias label: label.text
    property alias score: score.text
    property alias rectScore: score

    Rectangle {
        id: rectangle
        radius: 5
        border.width: 0
        anchors.fill: parent

        Row {
            id: row
            anchors.fill: parent
            spacing: 10
            anchors.margins: 10

            Text {
                id: label
                text: "Player"
                width: (iRoot.width - 30)/ 3 * 2
                anchors.verticalCenter: parent.verticalCenter
            }

            JMItem {
                id: score
                width: (iRoot.width - 30) / 3
                radius: 10
                text: "0"
                border.width: 0
                bottomColor: "#e5aa8e"
            }
        }
    }
    states: [
        State {
            name: "Selected"

            PropertyChanges {
                target: rectangle
                border.color: "#1ad836"
                border.width: 2
            }
        }
    ]
}
