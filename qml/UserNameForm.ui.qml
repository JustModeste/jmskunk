import QtQuick 2.4
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    id: iRoot
    width: 400
    height: 90

    property alias label:  label.text
    property alias name: name.text
    property alias isIA: switchIA.checked

    Column {
        id: column
        anchors.fill: parent
        spacing: 10

        Text {
            id: label
            text: "Player 1"
            width: iRoot.width
        }

        TextField {
            id: name
            width: iRoot.width
            placeholderText: qsTr("Player name")
        }

        Row {
            id: row
            spacing: 10
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                text: "Human"
                anchors.verticalCenter: parent.verticalCenter
            }
            Switch {
                id: switchIA
            }
            Text {
                text: "IA"
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }
}
